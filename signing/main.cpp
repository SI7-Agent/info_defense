#include "digital_sign.hpp"

int main()
{
    auto *reader = new Reader((char*)"C:/Users/Asus/CLionProjects/signing/config.conf");
    auto* worker = new DigitalSign(reader);

    if (reader->get_operation() == "sign")
        worker->sign();

    if (reader->get_operation() == "verify")
        worker->verify();

    delete reader;
    delete worker;

    return 0;
}