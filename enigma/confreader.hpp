#ifndef CONFREADER_HPP
#define CONFREADER_HPP

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

class Reader
{
private:
    char *m_config_name;

    std::string m_input;
    std::string m_encode;
    std::string m_decode;
    std::string m_state;
    std::string m_rings;

    int m_rotors;

    void init()
    {
        std::string buffer;
        std::ifstream config;
        config.open(this->m_config_name);

        while(getline(config, buffer))
        {
            std::string param;
            std::stringstream file_buffer(buffer);
            file_buffer >> param;

            if (param == "rotors")
            {
                file_buffer >> param;
                this->m_rotors = std::stoi(param);
                continue;
            }

            if (param == "ring")
            {
                file_buffer >> param;
                this->m_rings = param;
                continue;
            }

            if (param == "state_file")
            {
                file_buffer >> param;
                this->m_state = param;
                continue;
            }

            if (param == "file_in")
            {
                file_buffer >> param;
                this->m_input = param;
                continue;
            }

            if (param == "file_encode")
            {
                file_buffer >> param;
                this->m_encode = param;
                continue;
            }

            if (param == "file_decode")
            {
                file_buffer >> param;
                this->m_decode = param;
                continue;
            }
        }

        config.close();
    }

public:
    explicit Reader(char *config):
        m_config_name(config),
        m_input(std::string("")),
        m_encode(std::string("")),
        m_decode(std::string("")),
        m_state(std::string("")),
        m_rings(std::string("")),
        m_rotors(0)
    {
        this->init();
    }

    std::string get_input_name()
    {
        return this->m_input;
    }

    std::string get_encode_name()
    {
        return this->m_encode;
    }

    std::string get_decode_name()
    {
        return this->m_decode;
    }

    std::string get_state_name()
    {
        return this->m_state;
    }

    std::string get_rings()
    {
        return this->m_rings;
    }

    int get_rotors()
    {
        return this->m_rotors;
    }
};

#endif