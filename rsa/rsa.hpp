#ifndef RSA_HPP
#define RSA_HPP

#include <vector>
#include <ctime>
#include "confreader.hpp"

#define LIMIT 10000

class Rsa
{
public:
    explicit Rsa(Reader *config):
        m_decode_name(config->get_decode_name()),
        m_encode_name(config->get_encode_name()),
        m_input_name(config->get_input_name())
    {
        srand(time(NULL));
        this->generate_keys();
    }

    explicit Rsa(Reader *config, std::pair<int, int> &public_key, std::pair<int, int> &private_key):
            m_decode_name(config->get_decode_name()),
            m_encode_name(config->get_encode_name()),
            m_input_name(config->get_input_name()),
            m_public_key(public_key),
            m_private_key(private_key)
    {

    }

    void process(char code = 'e')
    {
        switch (code)
        {
            case 'e':
                std::cout << "ENCODE START\n";
                this->encode_file();
                std::cout << "ENCODE FINISH\n";
                break;

            case 'd':
                std::cout << "DECODE START\n";
                this->decode_file();
                std::cout << "DECODE FINISH\n";
                break;

            default:
                std::cerr << "Undefined option\n";
                break;
        }
    }

private:
    std::string m_input_name;
    std::string m_decode_name;
    std::string m_encode_name;

    std::pair<int, int> m_public_key;
    std::pair<int, int> m_private_key;

    void generate_keys()
    {
        int p, q;

        p = generate_prime();
        q = generate_prime();

        int n = p * q;
        int phi = (p - 1) * (q - 1);
        int e = generate_coprime(phi);

        this->m_public_key = std::make_pair(e, n);

        int d = modular_inverse(e, phi);

        this->m_private_key = std::make_pair(d, n);
    }

    void encode_file()
    {
        std::ifstream in(this->m_input_name, std::ios::binary | std::ios::in);
        std::ofstream out(this->m_encode_name, std::ios::binary | std::ios::out);

        char sym;
        while (in.get(sym))
        {
            int encoded_sym = this->encode_sym(sym);

            std::string encoded_sym_bits = this->num_to_bits(encoded_sym, 32);
            for (int sym_to_print = 0; sym_to_print < 4; sym_to_print++)
            {
                int to_print = this->bit_to_nums(encoded_sym_bits.substr(sym_to_print*8, 8));
                out << (char)to_print;
            }
        }

        in.close();
        out.close();
    }

    void decode_file()
    {
        std::ifstream in(this->m_encode_name, std::ios::binary | std::ios::in);
        std::ofstream out(this->m_decode_name, std::ios::binary | std::ios::out);

        char sym;
        int bytes = 0;
        std::string decoded_sym_bits = "";

        while (in.get(sym))
        {
            decoded_sym_bits += this->num_to_bits((unsigned)sym, 8);
            bytes++;

            if (bytes == 4)
            {
                int to_print = this->decode_sym(this->bit_to_nums(decoded_sym_bits));
                out << (char)to_print;

                decoded_sym_bits.clear();
                decoded_sym_bits = "";
                bytes = 0;
            }
        }

        in.close();
        out.close();
    }

    int encode_sym(int value)
    {
        return log_power(value, this->m_public_key.first, this->m_public_key.second);
    }

    int decode_sym(int value)
    {
        return log_power(value, this->m_private_key.first, this->m_private_key.second);
    }

    int log_power(int n, int p, int mod)
    {
        int result = 1;
        for (; p; p >>= 1)
        {
            if (p & 1)
                result = (1LL * result * n) % mod;
            n = (1LL * n * n) % mod;
        }
        return result;
    }

    bool rabin_miller(int n)
    {
        bool ok = true;

        for (int i = 1; i <= 5 && ok; i++)
        {
            int a = rand() + 1;
            int result = log_power(a, n - 1, n);
            ok &= (result == 1);
        }

        return ok;
    }

    int gcd(int a, int b)
    {
        while (b)
        {
            int r = a % b;
            a = b;
            b = r;
        }
        return a;
    }

    int generate_coprime(int n)
    {
        int generated = rand() % LIMIT;
        while (gcd(n, generated) != 1)
            generated = rand() % LIMIT;
        return generated;
    }

    std::pair<int, int> euclid_extended(int a, int b)
    {
        if(!b)
            return std::make_pair(1, 0);

        auto result = euclid_extended(b, a % b);
        return std::make_pair(result.second, result.first - (a / b) * result.second);
    }

    int modular_inverse(int n, int mod)
    {
        int inverse = euclid_extended(n, mod).first;
        while(inverse < 0)
            inverse += mod;
        return inverse;
    }

    int generate_prime()
    {
        int generated = rand() % LIMIT;
        while (!rabin_miller(generated))
            generated = rand() % LIMIT;

        return generated;
    }

    std::string num_to_bits(int num, int size)
    {
        std::string bit_size_input;

        for (int j = size - 1; j > -1; j--)
            bit_size_input.push_back(((num >> j) & 1) + '0');

        return bit_size_input;
    }

    int bit_to_nums(std::string bits)
    {
        int num = 0;

        for (int c : bits)
            num = (num << 1) | (c - '0');

        return num;
    }

    void save_key(std::string filename, std::vector<std::pair<std::string, int>> key_params)
    {
        std::ofstream out(filename, std::ios::out);

        for (auto param: key_params)
            out << param.first << " " << param.second;

        out.close();
    }

    std::vector<std::pair<std::string, int>> load_key(std::string filename)
    {
        std::ifstream in(filename, std::ios::in);
        std::string buffer;
        std::vector<std::pair<std::string, int>> key_params;

        while(getline(in, buffer))
        {
            std::string param, value;
            std::stringstream file_buffer(buffer);

            file_buffer >> param;
            file_buffer >> value;

            std::pair<std::string, int> p = std::make_pair(param, std::stoi(value));
            key_params.push_back(p);
        }

        in.close();

        return key_params;
    }
};

#endif