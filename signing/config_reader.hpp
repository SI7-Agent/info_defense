#ifndef CONFIG_READER_HPP
#define CONFIG_READER_HPP 

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

class Reader
{
private:
    char* m_config_name;

    std::string m_type;
    std::string m_signer;
    std::string m_filename;
    std::string m_sign;

    void init()
    {
        std::string buffer;
        std::ifstream config;
        config.open(this->m_config_name);

        while (getline(config, buffer))
        {
            std::string param;
            std::stringstream file_buffer(buffer);
            file_buffer >> param;

            if (param == "type")
            {
                file_buffer >> param;
                this->m_type = param;
                continue;
            }
            
            if (param == "signer")
            {
                file_buffer >> param;
                this->m_signer = param;
                continue;
            }

            if (param == "file")
            {
                file_buffer >> param;
                this->m_filename = param;
                continue;
            }

            if (param == "signature")
            {
                file_buffer >> param;
                this->m_sign = param;
                continue;
            }
        }

        config.close();
    }

public:
    explicit Reader(char* config) :
        m_config_name(config),
        m_type(std::string("")),
        m_signer(std::string("")),
        m_filename(std::string("")),
        m_sign(std::string(""))
    {
        this->init();
    }

    std::string get_operation()
    {
        return this->m_type;
    }
    
    std::string get_signer_name()
    {
        return this->m_signer;
    }

    std::string get_file_name()
    {
        return this->m_filename;
    }

    std::string get_signature_name()
    {
        return this->m_sign;
    }
};

#endif
