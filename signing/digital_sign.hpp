#ifndef DIGITAL_SIGN_HPP
#define DIGITAL_SIGN_HPP 

#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <wincrypt.h>
#include "config_reader.hpp"

#define CERT_PERSONAL_STORE_NAME  L"My"
#define MY_TYPE  X509_ASN_ENCODING
#define BUFSIZE 1024

class DigitalSign
{
public:
    DigitalSign(Reader* config):
        SignerName(this->trans_string(config->get_signer_name())),
        DataFileName(this->trans_string(config->get_file_name())),
        SignatureFileName(this->trans_string(config->get_signature_name()))
    {

    }

	void sign()
	{
        HCERTSTORE hStoreHandle = NULL;
        PCCERT_CONTEXT pSignerCert = NULL;
        HCRYPTPROV hCryptProv = NULL;
        DWORD dwKeySpec = 0;
        HCRYPTHASH hHash = NULL;
        HANDLE hDataFile = NULL;
        BOOL bResult = FALSE;
        BYTE rgbFile[BUFSIZE];
        DWORD cbRead = 0;
        DWORD dwSigLen = 0;
        BYTE* pbSignature = NULL;
        HANDLE hSignatureFile = NULL;
        DWORD lpNumberOfBytesWritten = 0;

        wprintf(L"SIGNING\n\n");

        hStoreHandle = CertOpenStore(
            CERT_STORE_PROV_SYSTEM,
            0,
            NULL,
            CERT_SYSTEM_STORE_CURRENT_USER,
            CERT_PERSONAL_STORE_NAME
        );
        CheckError((intptr_t)hStoreHandle, (wchar_t*)L"CertOpenStore....................... ");

        do {
            pSignerCert = CertFindCertificateInStore(
                hStoreHandle,
                MY_TYPE,
                0,
                CERT_FIND_SUBJECT_STR,
                SignerName,
                pSignerCert
            );
            CheckError((intptr_t)pSignerCert, (wchar_t*)L"CertFindCertificateInStore.......... ");

            bResult = CryptAcquireCertificatePrivateKey(
                pSignerCert,
                0,
                NULL,
                &hCryptProv,
                &dwKeySpec,
                NULL
            );
            CheckError(bResult, (wchar_t*)L"CryptAcquireCertificatePrivateKey... ");
        } while ((dwKeySpec & AT_KEYEXCHANGE) != AT_KEYEXCHANGE);

        bResult = CryptCreateHash(
            hCryptProv,
            CALG_SHA_256,
            0,
            0,
            &hHash
        );
        CheckError(bResult, (wchar_t*)L"CryptCreateHash..................... ");

        hDataFile = CreateFileW(DataFileName,
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            FILE_FLAG_SEQUENTIAL_SCAN,
            NULL
        );
        CheckError((hDataFile != INVALID_HANDLE_VALUE), (wchar_t*)L"CreateFile.......................... ");

        while (bResult = ReadFile(hDataFile, rgbFile, BUFSIZE, &cbRead, NULL))
        {
            if (cbRead == 0)
                break;

            CheckError(bResult, (wchar_t*)L"ReadFile............................ ");

            bResult = CryptHashData(
                hHash,
                rgbFile,
                cbRead,
                0
            );
            CheckError(bResult, (wchar_t*)L"CryptHashData....................... ");
        }

        CheckError(bResult, (wchar_t*)L"ReadFile............................ ");

        dwSigLen = 0;
        bResult = CryptSignHash(
            hHash,
            dwKeySpec,
            NULL,
            0,
            NULL,
            &dwSigLen
        );
        CheckError(bResult, (wchar_t*)L"CryptSignHash....................... ");

        pbSignature = (BYTE*)malloc(dwSigLen);
        CheckError((intptr_t)pbSignature, (wchar_t*)L"malloc.............................. ");

        bResult = CryptSignHash(
            hHash,
            dwKeySpec,
            NULL,
            0,
            pbSignature,
            &dwSigLen
        );
        CheckError(bResult, (wchar_t*)L"CryptSignHash....................... ");

        hSignatureFile = CreateFileW(
            SignatureFileName,
            GENERIC_WRITE,
            0,
            NULL,
            CREATE_ALWAYS,
            FILE_ATTRIBUTE_NORMAL,
            NULL
        );
        CheckError((hSignatureFile != INVALID_HANDLE_VALUE), (wchar_t*)L"CreateFile.......................... ");

        bResult = WriteFile(
            hSignatureFile,
            (LPCVOID)pbSignature,
            dwSigLen,
            &lpNumberOfBytesWritten,
            NULL
        );
        CheckError(bResult, (wchar_t*)L"WriteFile........................... ");

        free(pbSignature);
        CloseHandle(hDataFile);
        CloseHandle(hSignatureFile);

        bResult = CryptDestroyHash(hHash);
        CheckError(bResult, (wchar_t*)L"CryptDestroyHash.................... ");

        bResult = CertFreeCertificateContext(pSignerCert);
        CheckError(bResult, (wchar_t*)L"CertFreeCertificateContext.......... ");

        bResult = CertCloseStore(
            hStoreHandle,
            CERT_CLOSE_STORE_CHECK_FLAG
        );
        CheckError(bResult, (wchar_t*)L"CertCloseStore...................... ");
	}

	void verify()
	{
        HCERTSTORE hStoreHandle = NULL;
        PCCERT_CONTEXT pSignerCert = NULL;
        DWORD dwKeySpec = 0;
        HCRYPTPROV hCryptProv = NULL;
        HCRYPTHASH hHash = NULL;
        HANDLE hDataFile = NULL;
        BOOL bResult = FALSE;
        BYTE rgbFile[BUFSIZE];
        DWORD cbRead = 0;
        HANDLE hSignatureFile = NULL;
        BYTE* pbBinary = NULL;
        DWORD cbBinary = 0;
        HCRYPTKEY hPubKey = NULL;

        wprintf(L"VERIFYING\n\n");

        hStoreHandle = CertOpenStore(
            CERT_STORE_PROV_SYSTEM,
            0,
            NULL,
            CERT_SYSTEM_STORE_CURRENT_USER,
            CERT_PERSONAL_STORE_NAME
        );
        CheckError((intptr_t)hStoreHandle, (wchar_t*)L"CertOpenStore....................... ");

        pSignerCert = CertFindCertificateInStore(
            hStoreHandle,
            MY_TYPE,
            0,
            CERT_FIND_SUBJECT_STR,
            SignerName,
            pSignerCert
        );
        CheckError((intptr_t)pSignerCert, (wchar_t*)L"CertFindCertificateInStore.......... ");

        bResult = CryptAcquireContext(
            &hCryptProv,
            NULL,
            NULL,
            PROV_RSA_AES,
            CRYPT_VERIFYCONTEXT
        );
        CheckError(bResult, (wchar_t*)L"CryptAcquireContext................. ");

        bResult = CryptCreateHash(
            hCryptProv,
            CALG_SHA_256,
            0,
            0,
            &hHash
        );
        CheckError(bResult, (wchar_t*)L"CryptCreateHash..................... ");

        hDataFile = CreateFileW(
            DataFileName,
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            FILE_FLAG_SEQUENTIAL_SCAN,
            NULL
        );
        CheckError((hDataFile != INVALID_HANDLE_VALUE), (wchar_t*)L"CreateFile.......................... ");

        while (bResult = ReadFile(hDataFile, rgbFile, BUFSIZE, &cbRead, NULL))
        {
            if (cbRead == 0)
                break;

            CheckError(bResult, (wchar_t*)L"ReadFile............................ ");

            bResult = CryptHashData(
                hHash,
                rgbFile,
                cbRead,
                0
            );
            CheckError(bResult, (wchar_t*)L"CryptHashData....................... ");
        }

        CheckError(bResult, (wchar_t*)L"ReadFile............................ ");

        hSignatureFile = CreateFileW(
            SignatureFileName,
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            FILE_FLAG_SEQUENTIAL_SCAN,
            NULL
        );
        CheckError((hSignatureFile != INVALID_HANDLE_VALUE), (wchar_t*)L"CreateFile.......................... ");

        pbBinary = (BYTE*)malloc(BUFSIZE);
        CheckError((intptr_t)pbBinary, (wchar_t*)L"malloc.............................. ");

        bResult = ReadFile(hSignatureFile, pbBinary, BUFSIZE, &cbBinary, NULL);
        CheckError(bResult, (wchar_t*)L"ReadFile............................ ");

        CryptImportPublicKeyInfo(
            hCryptProv,
            MY_TYPE,
            &pSignerCert->pCertInfo->SubjectPublicKeyInfo,
            &hPubKey
        );
        CheckError(bResult, (wchar_t*)L"CryptImportPublicKeyInfo............ ");

        bResult = CryptVerifySignature(
            hHash,
            pbBinary,
            cbBinary,
            hPubKey,
            NULL,
            0
        );
        CheckError(bResult, (wchar_t*)L"CryptVerifySignature................ ");

        free(pbBinary);
        CloseHandle(hDataFile);
        CloseHandle(hSignatureFile);

        bResult = CryptDestroyHash(hHash);
        CheckError(bResult, (wchar_t*)L"CryptDestroyHash.................... ");

        bResult = CertFreeCertificateContext(pSignerCert);
        CheckError(bResult, (wchar_t*)L"CertFreeCertificateContext.......... ");

        bResult = CertCloseStore(
            hStoreHandle,
            CERT_CLOSE_STORE_CHECK_FLAG
        );
        CheckError(bResult, (wchar_t*)L"CertCloseStore...................... ");

        bResult = CryptReleaseContext(
            hCryptProv,
            0
        );
        CheckError(bResult, (wchar_t*)L"CryptReleaseContext................. ");
	}

private:
    wchar_t* SignerName;
    wchar_t* DataFileName;
    wchar_t* SignatureFileName;

    wchar_t* trans_string(std::string narrow_string)
    {
        int wchars_num = MultiByteToWideChar(CP_UTF8, 0, narrow_string.c_str(), -1, NULL, 0);
        wchar_t* wstr = new wchar_t[wchars_num];
        MultiByteToWideChar(CP_UTF8, 0, narrow_string.c_str(), -1, wstr, wchars_num);
        return wstr;
    }

    void CheckError(intptr_t condition, wchar_t* message)
    {
        wprintf(message);
        if (condition)
            wprintf(L"SUCCESS\n");

        else
        {
            wprintf(L"FAILURE (0x%x)\n", GetLastError());
            wprintf(L"\n<< Press any key to exit >>\n");
            _getch();
            exit(99);
        }
    }
};

#endif
