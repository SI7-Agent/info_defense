#include "confreader.hpp"
#include "des.hpp"

int main()
{
    auto *configs = new Reader((char*)"C:\\Users\\Asus\\CLionProjects\\des\\config.conf\0");

    auto *worker = new Des(configs);
    worker->process('e');
    worker->process('d');

    delete configs;
    delete worker;

    return 0;
}
