#include "machine.hpp"
#include "confreader.hpp"

int main()
{
    auto *configs = new Reader((char*)"C:\\Users\\Asus\\CLionProjects\\enigma\\config.conf\0");

    auto *worker_encode = new Machine(configs, 'e');
    worker_encode->process();

    //============================

    auto *worker_decode = new Machine(configs, 'd');
    worker_decode->process();

    return 0;
}