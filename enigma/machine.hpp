#ifndef MACHINE_HPP
#define MACHINE_HPP

#include "rotor.hpp"
#include "confreader.hpp"

class Machine
{
private:
    std::vector<int> m_buffer;

    std::string m_ring;
    std::string m_encode_name;
    std::string m_decode_name;
    std::string m_input_name;
    std::string m_state;

    int m_num_rotors;

    std::vector<Rotor*> m_rotors;
    Reflector *m_reflector;

    char m_key;

    void save_state(const char *name)
    {
        std::ofstream fd;
        fd.open(name);

        for (auto i : m_rotors)
            i->save_state(fd);

        this->m_reflector->save_state(fd);

        fd.close();
    }

    void load_state(const char *name)
    {
        std::string buffer;
        std::ifstream fd;
        fd.open(name);

        for (int i = 0; i < this->m_rotors.size(); i++)
        {
            getline(fd, buffer);
            this->m_rotors[i] = new Rotor(buffer);
        }

        getline(fd, buffer);
        this->m_reflector = new Reflector(buffer);
    }

    void randomize()
    {
        for (auto i = 0; i < this->m_rotors.size(); i++)
            this->m_rotors[i] = new Rotor(this->m_ring[i]);

        this->m_reflector = new Reflector();
    }

    void read_file(const char *name)
    {
        unsigned char byte;
        FILE *input = fopen(name, "rb");
        if(!input)
        {
            std::cout << "File does not exist\n";
            return;
        }

        this->m_buffer.clear();
        while (fread(&byte, sizeof(char), 1, input))
            this->m_buffer.push_back(int(byte));

        fclose(input);
    }

    void write_file(const char *name)
    {
        FILE * output = fopen(name, "wb");

        for (auto sym : this->m_buffer)
        {
            unsigned char byte = char(sym);
            fwrite(&byte, sizeof(char), 1, output);
        }

        fclose(output);
    }

    int code_one(int sym)
    {
        for (auto rotor : this->m_rotors)
            sym = rotor->get_value(sym);

        sym = this->m_reflector->get_value(sym);

        for (int i = this->m_rotors.size() - 1; i >= 0; i--)
            sym = this->m_rotors[i]->get_index(sym);

        this->spin_rotors();

        return sym;
    }

    void spin_rotors()
    {
        for (int i = 0, spin = 1; i < this->m_rotors.size() && spin; i++)
            spin = this->m_rotors[i]->spin();
    }

public:
    explicit Machine(Reader *config, char key):
        m_key(key),
        m_reflector(nullptr),
        m_ring(config->get_rings()),
        m_num_rotors(config->get_rotors()),
        m_rotors(std::vector<Rotor*>(this->m_num_rotors)),
        m_state(config->get_state_name()),
        m_decode_name(config->get_decode_name()),
        m_encode_name(config->get_encode_name()),
        m_input_name(config->get_input_name())
    {
        if (this->m_key == 'e')
        {
            if (this->m_ring.length() != this->m_num_rotors)
            {
                this->m_ring.clear();

                for (auto i = 0; i < this->m_num_rotors; i++)
                    this->m_ring += "A";

                this->randomize();
            }
            else
            {
                this->randomize();
            }

            this->save_state(this->m_state.c_str());
            std::cout << "Encode start OK\n";
        }
        else
        {
            this->load_state(this->m_state.c_str());
            std::cout << "Decode start OK\n";
        }
    }

    void process()
    {
        if (this->m_key == 'e')
            this->read_file(this->m_input_name.c_str());
        else
            this->read_file(this->m_encode_name.c_str());

        std::vector<int> encodeStr =  std::vector<int>(this->m_buffer);

        for (auto sym = this->m_buffer.begin(); sym != this->m_buffer.end(); sym++)
            *sym = code_one(*sym);

        if (this->m_key == 'e')
        {
            this->write_file(this->m_encode_name.c_str());
            std::cout << "Encode finish OK\n";
        }
        else
        {
            this->write_file(this->m_decode_name.c_str());
            std::cout << "Decode finish OK\n";
        }
    }

    void print_rotors_or_reflector(int index, char k = 'o')
    {
        if (index-1 < this->m_rotors.size() and index > -1 and k != 'r')
            if (k == 'a')
                for (int i = 0; i < index; i++)
                    this->m_rotors[i]->print();

            else if (k == 'o')
                this->m_rotors[index]->print();

            else
                std::cout << "Forbidden\n";

        else if (k == 'r')
            this->m_reflector->print();

        else
            std::cout << "Forbidden\n";
    }
};

#endif