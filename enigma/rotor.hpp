#ifndef ROTOR_HPP
#define ROTOR_HPP

#define DEPTH_ENCODE 256

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include <vector>

class Rotor
{
protected:
    int m_spin;
    int m_item;

    std::vector<int> m_transforming_table;

    void read_from_string(std::string line)
    {
        std::string connection;
        std::stringstream file_buffer(line);
        while(file_buffer >> connection)
            this->m_transforming_table.push_back(std::stoi(connection));
    }

    virtual void randomize()
    {
        for (int i = 0; i < DEPTH_ENCODE; i++)
        {
            this->m_transforming_table.push_back(i);
        }

        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine e(seed);
        std::shuffle(this->m_transforming_table.begin(), this->m_transforming_table.end(), e);
    }

    void set_m_spin_zero()
    {
        this->m_spin = 0;
    }

public:
    Rotor(int cur = 65):
        m_item(cur),
        m_spin(0)
    {
        this->randomize();

        while (m_item)
        {
            m_item -= 1;
            this->spin();
        }

        this->set_m_spin_zero();
    }

    Rotor(std::string line):
        m_item(0),
        m_spin(0)
    {
        this->read_from_string(line);
    }

    bool spin()
    {
        auto first = this->m_transforming_table[0];

        for (auto it = this->m_transforming_table.begin(); it != this->m_transforming_table.end()-1; it++)
            *it = *(it+1);

        *(this->m_transforming_table.end()-1) = first;

        if (this->m_spin < DEPTH_ENCODE)
            this->m_spin++;
        else
            this->m_spin = 0;

        return this->m_spin % DEPTH_ENCODE == 0;
    }

    void save_state(std::ofstream &fd)
    {
        for (auto iter = this->m_transforming_table.begin(); iter != this->m_transforming_table.end(); iter++)
            fd << *iter << " ";

        fd << "\n";
    }

    int get_value(int i)
    {
        return this->m_transforming_table[i];
    }

    int get_index(int value)
    {
        for (int i = 0; i < DEPTH_ENCODE; i++)
            if (this->m_transforming_table[i] == value)
                return i;

        return 0;
    }

    void print()
    {
        for (auto sym : m_transforming_table)
            std::cout << sym << " ";

        std::cout << "\n\n";
    }
};

class Reflector: public Rotor
{
protected:
    void randomize()
    {
        this->m_transforming_table.resize(DEPTH_ENCODE);

        for (int i = 0; i < DEPTH_ENCODE/2; i++)
        {
            this->m_transforming_table[i] = DEPTH_ENCODE/2 + i;
            this->m_transforming_table[DEPTH_ENCODE/2 + i] = i;
        }
    }

public:
    Reflector()
    {
        this->m_transforming_table.clear();
        this->randomize();
    }

    Reflector(std::string line)
    {
        this->m_transforming_table.clear();
        this->read_from_string(line);
    }
};

#endif