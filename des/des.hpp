#ifndef DES_HPP
#define DES_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "confreader.hpp"
#include "permutations.hpp"

class Des
{
public:
    explicit Des(Reader *config):
            m_decode_name(config->get_decode_name()),
            m_encode_name(config->get_encode_name()),
            m_input_name(config->get_input_name()),
            m_password(config->get_key()),
            m_tools(new Permutations)
    {
        this->m_password = this->transform(this->m_password);
    }

    void process(char code = 'e')
    {
        switch (code) 
        {
            case 'e':
                std::cout << "ENCODE START\n";
                this->encode_file();
                std::cout << "ENCODE FINISH\n";
                break;

            case 'd':
                std::cout << "DECODE START\n";
                this->decode_file();
                std::cout << "DECODE FINISH\n";
                break;

            default:
                std::cerr << "Undefined option\n";
                break;
        }
    }

private:
    Permutations *m_tools;

    std::string m_input_name;
    std::string m_decode_name;
    std::string m_encode_name;
    std::string m_password;

    void encode_file()
    {
        int size = this->m_tools->file_size(this->m_input_name);

        std::ifstream in(this->m_input_name, std::ios::binary | std::ios::in);
        std::ofstream out(this->m_encode_name, std::ios::binary | std::ios::out);

        char sym;
        std::string block = "";
        int i = 0;
        std::vector<std::string> round_keys = this->round_key_gen(this->m_password);

        std::string a = this->m_tools->num_to_bits(size, 32);
        std::string in_file_len = this->m_tools->pad_left(a, 64, '0');

        for (int j = 0; j < 8; j++)
        {
            unsigned char in_file_len_part = this->m_tools->bit_to_nums(in_file_len.substr(8*j, 8));
            out << in_file_len_part;
        }

        while (true)
        {
            bool eof = (bool)!in.get(sym);

            if (eof)
            {
                if (block.length() != 0)
                {
                    block = this->encode_block(this->m_tools->pad_right(block, 64, '0'), round_keys);
                    this->write_block(block, out);
                }

                break;
            }
            else
            {
                block += this->m_tools->pad_left(this->m_tools->num_to_bits(sym, 8), 8, '0');

                i += 1;
                if (i == 8)
                {
                    i = 0;

                    block = this->encode_block(block, round_keys);
                    this->write_block(block, out);

                    block.clear();
                    block = "";
                }
            }
        }

        in.close();
        out.close();
    }

    void decode_file()
    {
        int size = this->m_tools->file_size(this->m_encode_name);

        std::ifstream in(this->m_encode_name, std::ios::binary | std::ios::in);
        std::ofstream out(this->m_decode_name, std::ios::binary | std::ios::out);

        char sym;
        std::string block = "";
        std::vector<std::string> round_keys = this->round_key_gen(this->m_password);

        int in_file_len = size;
        std::string out_file_len = "";
        for (int j = 0; j < 8; j++)
        {
            in.get(sym);
            out_file_len += this->m_tools->pad_left(this->m_tools->num_to_bits(sym, 8), 8, '0');
        }
        int out_file_len_num = this->m_tools->bit_to_nums(out_file_len);
        int n = in_file_len/8 - 2;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                in.get(sym);
                block += this->m_tools->pad_left(this->m_tools->num_to_bits(sym, 8), 8, '0');
            }

            block = this->decode_block(block, round_keys);
            this->write_block(block, out);

            block.clear();
            block = "";
        }

        for (int j = 0; j < 8; j++)
        {
            in.get(sym);
            block += this->m_tools->pad_left(this->m_tools->num_to_bits(sym, 8), 8, '0');
        }

        block = this->decode_block(block, round_keys);
        this->write_block(block.substr(0, 64 - (in_file_len - out_file_len_num - 8)*8), out);

        in.close();
        out.close();
    }

    std::string transform(std::string key)
    {
        std::string transformed = "";
        unsigned long long n = key.length() > 16 ? 16 : key.length();

        for (int i = 0; i < n; i++)
        {
            switch(key[i])
            {
                case '0':
                    transformed += "0000";
                    break;

                case '1':
                    transformed += "0001";
                    break;

                case '2':
                    transformed += "0010";
                    break;

                case '3':
                    transformed += "0011";
                    break;

                case '4':
                    transformed += "0100";
                    break;

                case '5':
                    transformed += "0101";
                    break;

                case '6':
                    transformed += "0110";
                    break;

                case '7':
                    transformed += "0111";
                    break;

                case '8':
                    transformed += "1000";
                    break;

                case '9':
                    transformed += "1001";
                    break;

                case 'A':
                case 'a':
                    transformed += "1010";
                    break;

                case 'B':
                case 'b':
                    transformed += "1011";
                    break;

                case 'C':
                case 'c':
                    transformed += "1100";
                    break;

                case 'D':
                case 'd':
                    transformed += "1101";
                    break;

                case 'E':
                case 'e':
                    transformed += "1110";
                    break;

                case 'F':
                case 'f':
                default:
                    transformed += "1111";
                    break;
            }
        }

        return this->m_tools->pad_left(transformed, 64, '0');
    }

    std::string odd_key_bytes(std::string key)
    {
        std::string new_key;
        int ones;

        for (int i = 0; i < 8; i++)
        {
            ones = 0;

            for (int j = 0; j < 8; j++)
                if (j == 7)
                {
                    if (ones % 2 == 0)
                        new_key += '1';
                    else
                        new_key += '0';
                }
                else
                {
                    new_key += key[i * 8 + j];
                    if (key[i * 8 + j] == '1')
                        ones += 1;
                }
        }

        return new_key;
    }

    std::vector<std::string> round_key_gen(std::string key)
    {
        std::string new_key = this->odd_key_bytes(key);
        std::vector<std::string> round_keys(16);

        int pos = 0;

        for (int i = 0; i < 16; i++)
        {
            pos += this->m_tools->make_si(i);
            std::string temp_key = "";
            round_keys[i] = "";

            for (int j = 0; j < 56; j++)
            {
                if (j < 28)
                    temp_key += new_key[this->m_tools->make_c0((j + pos) % 28) - 1];
                else
                    temp_key += new_key[this->m_tools->make_d0((j - 28 + pos) % 28) - 1];
            }

            for (int j = 0; j < 48; j++)
                round_keys[i] += temp_key[this->m_tools->make_cp(j) - 1];
        }

        return round_keys;
    }

    void write_block(std::string block, std::ofstream &file)
    {
        int n = block.length() / 8;
        for (int i = 0; i < n; i++)
        {
            unsigned char sym = this->m_tools->bit_to_nums(block.substr(i*8, 8));
            file << sym;
        }
    }

    std::string encode_block(std::string block, std::vector<std::string> round_keys)
    {
        block = this->m_tools->make_ip(block);

        for (int i = 0; i < 16; i++)
            block = this->feistel(block, round_keys[i]);

        block = block.substr(32, 32) + block.substr(0, 32);
        block = this->m_tools->make_ip_inv(block);

        return block;
    }

    std::string decode_block(std::string block, std::vector<std::string> round_keys)
    {
        block = this->m_tools->make_ip(block);

        for (int i = 0; i < 16; i++)
            block = this->feistel(block, round_keys[15 - i]);

        block = block.substr(32, 32) + block.substr(0, 32);
        block = this->m_tools->make_ip_inv(block);

        return block;
    }

    std::string feistel(std::string block, std::string key)
    {
        std::string left;
        std::string right;
        std::string res = "";

        left = block.substr(0, 32);
        right = block.substr(32, 32);

        res += right;
        res += this->my_xor(left, this->f(right, key));

        return res;
    }

    std::string my_xor(std::string val1, std::string val2)
    {
        std::string c = "";
        int n = val1.length();

        for (int i = 0; i < n; i++)
        {
            if (val1[i] == val2[i])
                c += '0';
            else
                c += '1';
        }

        return c;
    }

    std::string f(std::string block, std::string key)
    {
        block = this->m_tools->make_e(block);

        std::string b = this->my_xor(block, key);
        std::string b_new = "";

        for (int j = 0; j < 8; j++)
        {
            std::string bj = b.substr(j*6, 6);

            std::string x_str = bj.substr(0, 1) + bj.substr(5, 1);
            std::string y_str = bj.substr(1, 4);
            int x_num = this->m_tools->bit_to_nums(x_str);
            int y_num = this->m_tools->bit_to_nums(y_str);

            int bj_new_num = this->m_tools->make_sbox(j, x_num, y_num);
            std::string bj_new = this->m_tools->pad_left(this->m_tools->num_to_bits(bj_new_num, 4), 4, '0');
            b_new += bj_new;
        }

        b_new = this->m_tools->make_p(b_new);
        return b_new;
    }
};

#endif