#ifndef CONFREADER_HPP
#define CONFREADER_HPP

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

class Reader
{
private:
    char *m_config_name;

    std::string m_input;
    std::string m_encode;
    std::string m_decode;
    std::string m_key;

    void init()
    {
        std::string buffer;
        std::ifstream config;
        config.open(this->m_config_name);

        while(getline(config, buffer))
        {
            std::string param;
            std::stringstream file_buffer(buffer);
            file_buffer >> param;

            if (param == "secret_key")
            {
                file_buffer >> param;
                this->m_key = param;
                continue;
            }

            if (param == "file_in")
            {
                file_buffer >> param;
                this->m_input = param;
                continue;
            }

            if (param == "file_encode")
            {
                file_buffer >> param;
                this->m_encode = param;
                continue;
            }

            if (param == "file_decode")
            {
                file_buffer >> param;
                this->m_decode = param;
                continue;
            }
        }

        config.close();
    }

public:
    explicit Reader(char *config):
            m_config_name(config),
            m_input(std::string("")),
            m_encode(std::string("")),
            m_decode(std::string("")),
            m_key(std::string(""))
    {
        this->init();
    }

    std::string get_input_name()
    {
        return this->m_input;
    }

    std::string get_encode_name()
    {
        return this->m_encode;
    }

    std::string get_decode_name()
    {
        return this->m_decode;
    }

    std::string get_key()
    {
        return this->m_key;
    }
};

#endif