#include <iostream>

#include "rsa.hpp"
#include "confreader.hpp"

int main()
{
    auto *configs = new Reader((char*)"C:\\Users\\Asus\\CLionProjects\\rsa\\config.conf\0");

    auto *worker = new Rsa(configs);
    worker->process('e');
    worker->process('d');

    delete configs;
    delete worker;

    return 0;
}